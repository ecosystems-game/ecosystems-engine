#include "eco-system/Entity.hpp"
#include "eco-system/Scene.hpp"


namespace ecosystems::system {
    Entity::Entity(entt::entity p_handle, Scene *p_scene)
        : m_entity_handle(p_handle), m_scene(p_scene){}
}