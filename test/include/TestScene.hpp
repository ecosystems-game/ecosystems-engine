#ifndef ECOSYSTEMS_TESTSCENE_HPP
#define ECOSYSTEMS_TESTSCENE_HPP

#include <iostream>

#include <eco-system/Scene.hpp>
#include <eco-system/Entity.hpp>
#include <eco-system/components/BehaviorComponent.hpp>

#include "behaviors/TestBehavior.hpp"


namespace ecosystems::system::test::scenes {
    class TestScene : public Scene {
    public:
        explicit TestScene() = default;
        void mounted() override {
            auto entity = createEntity("test-entity");
            entity.addComponent<components::BehaviorComponent>().bind<behaviors::TestBehavior>();
        }

        void update(float p_delta_time) override {
            Scene::update(p_delta_time);
        }
    };
}

#endif //ECOSYSTEMS_TESTSCENE_HPP
