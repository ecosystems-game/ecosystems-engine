#ifndef ECOSYSTEMS_ENTITY_HPP
#define ECOSYSTEMS_ENTITY_HPP

#include <memory>
#include <iostream>
#include <thirdparty/entt.hpp>
#include "Scene.hpp"

namespace ecosystems::system {

    class Entity {
    public:
        explicit Entity() = default;
        Entity(entt::entity p_handle, Scene* p_scene);
        Entity(const Entity& other) = default;

        template<typename T, typename... Args>T& addComponent(Args&&... args) {
            return m_scene->m_registry.emplace<T>(m_entity_handle, std::forward<Args>(args)...);
        }

        template<typename T> T& getComponent() {
            return m_scene->m_registry.get<T>(m_entity_handle);
        }

        template<typename T> void removeComponent() {
            return m_scene->m_registry.remove<T>(m_entity_handle);
        }
        template<typename T> bool hasComponent() {
            return m_scene->m_registry.any_of<T>(m_entity_handle);
        }

        operator bool() const { return m_entity_handle != entt::null; }
    private:
        entt::entity m_entity_handle{0};
        Scene* m_scene = nullptr;
    };

}

#endif //ECOSYSTEMS_ENTITY_HPP
