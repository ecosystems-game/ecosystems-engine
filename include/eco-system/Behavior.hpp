#ifndef ECOSYSTEMS_BEHAVIOR_HPP
#define ECOSYSTEMS_BEHAVIOR_HPP

#include "Entity.hpp"

namespace ecosystems::system {
    class Scene;
    class Behavior {
    public:
        template<typename T> T& getComponent() {
            return m_entity.getComponent<T>();
        }
    private:
        Entity m_entity;
        friend class Scene;
    };
}

#endif //ECOSYSTEMS_BEHAVIOR_HPP
